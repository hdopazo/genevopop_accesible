# Glosario de Términos y Abreviaturas 

## 0123...
1. **1,000 Genomes Project (1KGP)**. An international research consortium that will sequence the genomes of 1,200 individuals of various ethnicities. Most individuals will be sequenced to low coverage, or in exons only. The goals are to catalogue human variation with minor allele frequencies of ~1% or greater and to refine and optimise strategies for sequencing large numbers of genomes.

## A
1. **Absolute risk**: Is the unqualified probability, or risk, that a certain event will occur; it ranges from 0–100%.

1. **Acrocentric chromosome**. A chromosome that has a centromere at or close to one end. Human acrocentric chromosomes are 13, 14, 15, 21 and 22. 

1. **Additive genetic variance**. The part of the total genetic variation that is due to the main (or additive) effects of alleles on a phenotype. The additive variance determines the response to selection.

1. **Admixture**. The mixture of two or more genetically distinct populations. The introduction of mates (more properly, their genes) from one previously distinct population (the gene frequencies of which might differ) into another.

1. **Allele**. An alternative form of a gene or SNP, or another type of variant.

1. **Allele Drop-Out (ADO)**. The failure to detect an allele in a sample or the failure to amplify an allele during PCR.

1. **Allele-specific expression**. Expression variation between the two haplotypes of a diploid individual, as distinguished by heterozygous sites.

1. **Allelic heterogeneity**. Refers to the number of different mutations at a single gene that can influence risk of disease. 

1. **Allelic Phase**. The information that is needed to determine the two haplotypes that underlie a multi-locus genotype within a chromosomal segment.

1. **Alpha (a)**: the estimated fraction of substitutions that were driven by positive selection, and for nonsynonymous substitutions can be estimated by 1-( pN/ pS)/(dN/dS).

1. **Alu**. A family of short interspersed nuclear elements that are common in human and primate genomes.

1. **Amplicon**. The repeat unit, or unit length of genome, that is amplified.

1. **AIMs. Ancestry Informative Markers**.  Genetic markers that show substantial differences in allele frequency across population groups.

1. **Ancestry principal components**: Principal components derived from the genome relationship matrix that account for the genetic substructure of the data. In case–control studies, these principal components can reflect genotyping artefacts, such as plate, batch and genotyping centre, that could be confounded with case–control status.

1. **Aneuploidy, aneusomy and heteromorphism**. These terms have origins in classical cytogenetics and describe structural variants at the largest end of the scale. Aneuploidy is the state of having an abnormal number of chromosomes. Similarly, segmental aneusomy, in reference to a portion of a chromosome, implies abnormality. Heteromorphism (literally, ‘different form’) has come to imply normal variation, or an atypical chromosome form not associated with an abnormal phenotype. Such large-scale variants are often the basis for dysfunction owing to dosage imbalance (such as for segmental aneusomy syndromes), but may also be part of normal functional variation.

1. **Ancient repeat. AR**. A repeat that was inserted into the early mammalian lineage and has since become dormant; the majority of ancient repeats are thought to be neutrally evolving.

1. **Archaic humans**. A broad category of human populations that diverged from present-day humans 550–765 thousand years ago (kya) (assuming a mutation rate of 0.5×10−9 per base pair per year) before present-day human populations started diverging from each other 86–130 kya (assuming the same mutation rate) and that are now extinct. This includes the Neanderthal and Denisovan populations. 

1. **Array comparative genomic hybridization (Array CGH)**. A technique based on competitively hybridizing fluorescently labelled test and reference samples to a known target DNA sequence immobilised on a solid glass substrate and then interrogating the hybridization ratio. A microarray-based method for detecting copy number variation in the genome.

1. **Assembly**: A set of sequences (chromosomes, unlocalized, unplaced, and alternate loci) used to represent an organism’s genome. 

1. **Assembly Unit**: Collections of sequences used to define discrete parts of an assembly. 

1. **Ascertainment bias**. The bias in patterns of variation that results from using pre-ascertained SNPs. A consequence of collecting a nonrandom subsample with a systematic bias so that results based on the subsample are not representative of the entire sample.

1. **Association studies**. A gene-discovery strategy that compares allele frequencies in cases and controls to assess the contribution of genetic variants to phenotypes in specific populations.

1. **Autozygosity**: homozygosity in which the two alleles are identical by descent (ie they are copies of an ancestral gene).

## B

1. **BAC-by-BAC sequencing**. A sequencing method where a physical map is generated from overlapping bacterial artificial chromosome (BAC) clones tiled across a chromosome. Each BAC is then fragmented and sequenced. The sequenced fragments are aligned with the knowledge of the originating BAC. 

1. **BAC clone**: Bacterial artificial chromosome vector carrying a genomic DNA insert, typically 100±200 kb. Most of the large-insert clones sequenced in the HGP were BAC clones.

1. **Background selection**. The process of natural selection by which the outstanding pattern is the observation of neutral variants linked to new deleterious mutations. The elimination of neutral polymorphisms as a result of the negative selection of deleterious mutations at linked sites.

1. **Balancing Selection**. A selection regime that results in the maintenance of two or more alleles at a single locus in a population.

1. **Barcodes**. A series of known bases added to a template molecule either through ligation or amplification. After sequencing, these barcodes can be used to identify which sample a particular read is derived from. 

1. **Batch effect**: Systematic biases in the data that arise from differences in sample handling.

1. **Bayesian**. A statistical school of thought that, in contrast to the frequentist school, holds that inferences about any unknown parameter or hypothesis should be encapsulated in a probability distribution, given the observed data. Bayes theorem is a celebrated result in probability theory that allows one to compute the posterior distribution for an unknown from the observed data and its assumed prior distribution. 

1. **Bayesian Approach**. A statistical approach that, given a set of assumptions about the underlying model, can provide a rigorous assessment of uncertainty.

1. **Biallelic**, having only two possible alleles in a variant, typically a SNP.

1. **Blastocyst**. A preimplantation embryo that contains a fluid-filled cavity called a blastocoel.

1. **Blastomere**. A cell that results from embryonic cleavage. 

1. **Bonferroni or Hochberg corrections**. Statistical methods, proposed by Bonferroni and Hochberg, for controlling type-1error (false positives) in the presence of multiple testing.

1. **Bootstrap**. A type of statistical analysis that is generally used for measuring the reliability of a sample estimate. It proceeds by the repeated sampling, with replacement, of the original data set. In the application described here, bootstrapping is used to assess the probability of identifying the causal variant for a genetic condition in a population. 

1. **Broad-sense phenotypic heritability**: The proportion of trait variance that is due to all genetic factors, including dominant and recessive factors, as well as the interactions between genetic factors. Narrow-sense heritability is the proportion of trait variance that is due to additive genetic factors.

1. **Bottleneck**: A temporary reduction in population size that causes the loss of genetic variation.

## C

1. **CAGE**.  Cap analysis of gene expression (CAGE). Capture of the methylated cap at the 5' end of RNA, followed by high-throughput sequencing of a small tag adjacent to the 5' methylated caps. 5' methylated caps are formed at the initiation of transcription, although other mechanisms also methylate 5' ends of RNA.

1. **CAGE tag**: A short sequence from the 5' end of a transcript.

1. **Candidate gene**. A gene for which there is evidence, usually functional, for a possible role in a disease or trait of interest. 

1. **Case–cohort study**. Similar to a case–control study, except both cases and controls are drawn from an existing cohort of subjects who are being followed to study a broad spectrum of diseases and risk factors. | An epidemiological study design in which cases with a defined condition and controls without this condition are sampled from the same population. Risk-factor information is compared between the two groups to investigate the potential role of these in the aetiology of the condition. 

1. **Centimorgan (cM**). In genetics, a centimorgan or map unit (m.u.) is a unit for measuring genetic linkage. It is defined as the distance between loci or markers for which the expected average number of intervening chromosomal crossovers in a single generation is 0.01(1%). It is often used to infer distance along a chromosome. In humans, 1 cM is approximately 1 million base pairs (bp) of DNA.

1. **CEPH**: Centre d’E ́ tude du Polymorphisme Humain (Paris, France).

1. **CDS Coding sequence**: a region of a cDNA or genome that encodes proteins

1. **ChIP-seq**. Chromatin immunoprecipitation followed by sequencing. Specific regions of crosslinked chromatin, which is genomic DNA in complex with its bound proteins, are selected by using an antibody to a specific epitope. The enriched sample is then subjected to high- throughput sequencing to determine the regions in the genome most often bound by the protein to which the antibody was directed. Most often used are antibodies to any chromatin-associated epitope, including transcription factors, chromatin binding proteins and specific chemical modifications on histone proteins.

1. **ChIP-chip**: Chromatin immunoprecipitation followed by detection of the products using a genomic tiling array

1. **Chromatin domains**. Functionally distinct chromosomal regions that confer structural organization to eukaryotic genomes and represent regulatory units for gene expression and chromosome behaviour. 

1. **Chromosome banding**. A method of defining chromosome structure by staining with Giemsa and looking at the banding pattern in the heterochromatin of the centromeric regions.

1. **Cis-regulatory elements**. Genomic regions (for example, enhancers) that regulate the expression of genes on the same chromosome.

1. **Circulating tumor cells (CTCs)**: cells shed by the primary tumor into the bloodstream very early in tumor development.

1. **Circulating cell-free tumor DNA**: DNA released by apoptotic and necrotic cells of the primary tumor into the blood circulation early in tumor development.

1. **Clusters**. Groups of DNA templates in close spatial proximity, generated either though bead-based amplification or by solid-phase amplification. Bead-based approaches rely on emulsions to maintain template isolation during amplification. Solid-phase approaches rely on the template-to-bound-adapter ratio to probabilistically bind template molecules at a sufficient distance from each other. 

1. **Cluster density**. The density of clonal double-stranded DNA fragment clusters bound to an Illumina flow cell, typically expressed as clusters per mm2. It is used as a quality-control metric early during the sequencing reaction: low cluster densities will result in a lower sequencing yield in the resulting fastq library, whereas very high cluster densities will result in poor sequence quality. 

1. **Coalescence**. The merging of ancestral lineages going back in time.

1. **Coalescent simulation**. A method of simulating data under a population genetic model.

1. **Cochran-armitage trend test**. Statistical test for analysis of categorical data when categories are ordered. It is used to test for association in a 2 × k contingency table (k > 2). In genetic association studies, because the underlying genetic model is unknown, the additive version of this test is most commonly used

1. **Cohort**. It is a group of people who share a common characteristic or experience within a defined period (e.g., are exposed to a drug or vaccine or pollutant, or undergo a certain medical procedure).

1. **Cohort study**. A form of longitudinal study (a type of observational study) used in medicine, social science, business analytics, and ecology. In medicine, a cohort study is often undertaken to obtain evidence to try to refute the existence of a suspected association between cause and effect; failure to refute a hypothesis often strengthens confidence in it.

1. **Colocalizing**: Different genetic variants in high linkage disequilibrium located in the same gene that affect different phenotypes.

1. **Common-disease, common-variant hypothesis**: The hypothesis that many genetic variants that underlie complex diseases are common, and therefore susceptible to detection using current population association study designs. An alternative possibility is that genetic contributions to complex diseases arise from many variants, all of which are rare. 

1. **Complex traits**: Traits that do not follow Mendelian inheritance patterns and are derived from any combination of multiple genetic factors, environmental factors and their interactions.

1. **Comorbidity**. Comorbidity implies the presence of one or more disorders (or diseases) in addition to a primary disease or disorder that the patient has. Comorbidity may hide causal effects, when one disease enhances the emergence of some other disease, such as the much-studied comorbidity between diabetes and obesity. 

1. **Compound heterozygote**. When an individual inherits two different recessive mutations, one from each parent, in the same gene that cause the same phenotype. An example would be a single-nucleotide variant causing a codon for an amino acid to be changed into a stop codon in one allele and a 4-bp deletion in the other allele: each of these variants knock out their respective allele, resulting in neither copy functioning.

1. **Confidence interval**. The range of values surrounding a point estimate, such as an OR, within which the true value is believed    to lie with a specified degree of certainty (typically 95%).

1. **Confounding**. A type of bias in statistical analysis that occurs when a factor exists that is causally associated with the outcome under study (e.g., case-control status) independently of the exposure of primary interest (e.g., the genotype at a given locus) and is associated with the exposure variable but is not a consequence of the exposure variable.

1. **Confounding factor**: A variable (for example, batch effects or population structure) that is associated with both the genotype and the phenotype of interest and can give rise to a spurious association.

1. **Constitutional translocations**. Chromosome abnormalities that occur before fertilization (during meiosis) or early in embryogenesis (during mitosis), such that essentially all cells in the individual harbour the same abnormality. 

1. **Constrained sequence (CS**): a genomic region associated with evidence of negative selection (that is, rejection of mutations relative to neutral regions)

1. **Contig**. The result of joining an overlapping collection of sequences or clones.

1. **Copy number variation /variant (CNV)**. We use these terms to refer to a DNA segment of at least 1 kb in size, for which copy number differences have been observed in the comparison of two or more genomes. Without further annotation, CNV carries no implication of relative frequency or phenotypic effect. These quantitative variants can be genomic copy number gains (insertions or duplications) or losses (deletions or null genotypes) relative to a designated reference genome sequence. A copy number polymorphism (CNP) is a CNV that occurs in more than 1% of the population | Merging of independently ascertained, but overlapping, genomic segments creates the representation of a CNV locus (that is, a segment at a fixed chromosomal position); the accumulation of data gradually will reveal the true underlying structure of the variant segment. In some cases, this will be a discrete cassette of DNA; in others, it will be a multiplex arrangement of variant units in close proximity, forming a CNV region11. A given variable segment can be detected with multiple clones in a single array or by different arrays in different studies, and its borders gradually fine-tuned with targeted assays. By their very nature, these segments may have different forms among the individuals used for their discovery.

1. **Core promoter**. The genomic region that surrounds a TSS (Transcription Start Site) or cluster of TSSs. There is no absolute definition for the length of a core promoter; it is generally defined empirically as the segment of DNA that is required to recruit the transcription initiation complex and initiate transcription, given the appropriate external signals (such as enhancers) 

1. **Core region**. It is a small region of a chromosomes genotyped with a dense collection of SNPs used to identify ‘core haplotypes’. SNPs in the core region are of sufficient density, so that recombination between them would be extremely rare and the core haplotypes could be explained in terms of a single gene genealogy. 

1. **Co-segregation**. In the pedigree of a family with a condition, the segregation pattern shows how often the putative causal variant is found to coincide with the condition. When a variant coincides with the condition in a family, the condition and the variant are said to co-segregate.

1. **Cosmopolitan populations**. Populations that are not isolated; typical urban populations.

1. **COT-1 DNA**. DNA that is mainly composed of repetitive sequences. It is produced when short fragments of denatured genomic DNA are re-annealed.

1. **Coverage**. The number of sequence reads that have alignments that overlap a certain.

1. **crRNA arrays**. in bacterial genomes, series of spacers flanked by repeats, which are transcribed as a single pre-CRisPR RNA array and subsequently processed into individual CRisPR RNAs

1. **CRISPR–Cas**. (Clustered regularly interspaced short palindromic repeat–CRISPR-associated). A technique for generating site-specific mutant or transgenic organisms using the Cas9 protein–guide RNA complex to generate mutations or to direct exogenous DNA to specific genomic regions where it is incorporated.

1. **Crossover**. A precisely reciprocal breakage of two DNA molecules followed by rejoining with exchanged partners.

1. **Cryptic translocation**. A translocation, particularly one that involves DNA near telomeres, that is too small to be detectable by traditional chromosome-banding analysis.

## D

1. **Degree**. The degree of a node is the number of links that connect to it. The degree of a protein could represent the number of proteins with which it interacts with, whereas the degree of a disease may represent the number of other diseases that are associated with the same gene or that have a common phenotype. 

1. **Degrees of freedom**. This term is used in different senses both within statistics and in other fields. It can often be interpreted as the number of values that can be defined arbitrarily in the specification of a system; for example, the number of coefficients in a regression model. It is often sufficient to regard degrees of freedom as a parameter that is used to define particular probability distributions. 

1. **Demographic parameters**: parameters that define a population’s size and structure over time (e.g., divergence times, rates of migration and growth, and the severity/duration of past bottlenecks)

1. **Derivative chromosome**. An abnormal chromosome consisting of segments of two or more chromosomes joined together as a result of a translocation of other rearrangement.

1. **Deep imputation**: The use of large imputation reference panels to accurately estimate most low-frequency (minor allele frequency (MAF) ≥1% but ≤5%) and some rare (MAF <1%) unobserved genetic variation in individuals who have undergone genome-wide genotyping.>

1. **Dicentric chromosome**. A chromosome with two centromeres. These are pulled to opposite poles during mitosis but are unable to separate without chromosome breakage.

1. **Directed evolution**:  Method of generating and selecting for nucleic acid or protein variants with desirable properties.
 
1. **Directional selection**. Natural selection that favours values of a quantitative trait at one extreme of the population distribution. In positive directional selection, natural selection favours values of a quantitative trait at the upper extreme of the population distribution.

1. **dN**: number of nonsynonymous mutations per nonsynonymous site.

1. **dS**: number of synonymous mutations per synonymous site.

1. **dN /dS ratio**: rate ratio of nonsynonymous to synonymous substitutions.

1. **DNA exaptation**. The shift in the function of a DNA sequence during evolution. 

1. **DNase-seq**. Adaption of established regulatory sequence assay to modern techniques. The DNase I enzyme will preferentially cut live chromatin preparations at sites where nearby there are specific (non- histone) proteins. The resulting cut points are then sequenced using high-throughput sequencing to determine those sites ‘hypersensitive’ to DNase I, corresponding to open chromatin.

1. **DNaseI hypersensitive site (DHS)**: a region of the genome showing a sharply different sensitivity to DNaseI compared with its immediate locale.

1. **Double minute**. Acentric, extra-chromosomally amplified chromatin, which usually contains a particular chromosomal segment or gene; common in cancer cells.

1. **Double-stranded end**. An end of dsDNA that is not protected by a telomere, which is the structure found at the ends of linear chromosomes.

1. **Draft genome sequence**: The sequence produced by combining the information from the individual sequenced clones (by creating merged sequence contigs and then employing linking information to create scaffolds) and positioning the sequence along the physical map of the chromosomes.

## E

1. **Edgetic**. Edgetic perturbations denote mutations that do not result in the complete loss of a gene product, but affect one or several interactions (and thus functions) of a protein. From a network perspective, an edgetic perturbation removes one or several links, but leaves the other links and the node unaffected. 

1. **Effect size**. The increase in risk (or proportion of population variation) that is conferred by a given causal variant. **or** The percentage of phenotypic variance that can be attributed to a given genomic variant.

1. **Effective population size (Ne)**. The number of breeding individuals in an idealised population that would show the same amount of dispersion of allele frequencies under random genetic drift or the same amount of inbreeding as the population under consideration. | This term usually refers to the size of an ideal population with the same rate of genetic drift of gene frequencies as the actual population. For example, if the population size fluctuated, the effective population size is equal to the harmonic mean of the population size.

1. **Emulsion picolitre droplet PCR**. Emulsion PCR is based on the generation of independent PCR reaction by emulsifying the aqueous reagents in oil such that each droplet becomes a separate PCR reaction. Reagents are diluted such that each droplet contains a single target sequence.

1. **Endonuclease**. An enzyme that breaks the sugar–phosphate backbone of a DNA or rNA molecule where there is no free end.

1. **Endophenotype**. An intermediate phenotype that is heritable and associated with a disease but is not itself a symptom of the disease. Although there is little evidence to support the theory, it has been argued that endophenotypes would be a more tractable target for genetic analysis than the relevant disease state itself.

1. **EST Expressed sequence tag**: a short sequence of a cDNA indicative of expression at this point.

1. **Estimator**: A function that produces an estimate of some parameter.

1. **Euchromatic variant**. A subset of cytogenetic heteromorphisms that involve microscopically visible variations of the euchromatic regions of chromosomes.

1. **Epidemiology**. The science that studies the patterns, causes, and effects of health and disease conditions in defined populations.

1. **Epigenetic**. Pertaining to an inherited phenotypic change: caused by mechanisms other than changes in the underlying DNA sequence, such as DNA methylation or histone modification.

1. **Epistasis**: a statistical interaction between genotypes at two or more loci. Although epistasis is used in many different contexts, here it is used to refer to mutations with effects that depend on genotypes at other loci.

1. **Epistatic interactions**. Modification of the expressivity of an allele by one or several other genes (known as ’modifier genes’). In Crohn’s disease, epistatic interactions may explain up to 80% of the so-called ‘missing heritability’.

1. **Etiology**: The combination of factors that give rise a disease.

1. **Exonuclease**. An enzyme that degrades DNA or rNA from an end.

1. **Exome**. The exome is the collection of known exons in our genome: this is the portion of the genome that is translated into proteins. An exome accounts for about 180,000 exons and for around 1% of the human genome which translates to about 30 megabases (Mb) in length, and contain the most easily understood, functionally relevant information. 

1. **Exome sequencing**: also known as targeted exome capture, this method is an efficient strategy to selectively sequence the exons and flanking intronic sections of the human genome to identify novel genes associated with rare and common disorders. It is estimated that about 85% of disease-causing mutations lie within the protein-coding regions of the human genome.

1. **Expression quantitative trait locus (eQTL)**. A genomic locus that regulates the mRNA expression level of a gene. | Genomic loci that contribute to variation in the expression levels of mRNAs.

1. **Extended haplotype homozygosity and relative EHH**. EHH at a distance x from the core region is defined as the probability that two randomly chosen chromosomes carrying a tested core haplotype are homozygous at all SNPs for the entire interval from the core region to the distance x. EHH is on a scale of 0 (no homozygosity, all extended haplotypes are different) to 1 (complete homozygosity, all extended haplotypes are the same). Relative EHH is the ratio of the EHH on the tested core haplotype compared with the EHH of the grouped set of core haplotypes at the region not including the core haplotype tested. Relative EHH is therefore on a scale of 0 to infinity

## F

1. **FAIRE Formaldehyde-assisted isolation of regulatory elements**: a method to assay open chromatin using formaldehyde crosslinking followed by detection of the products using a genomic tiling array. FAIRE isolates nucleosome-depleted genomic regions by exploiting the difference in crosslinking efficiency between nucleosomes (high) and sequence-specific regulatory factors (low). FAIRE consists of crosslinking, phenol extraction, and sequencing the DNA fragments in the aqueous phase.

1. **False discovery rate**. The proportion of non-causal or false positive significant SNPs in a genetic association study. (FDR) a statistical method for setting thresholds on statistical tests to correct for multiple testing.

1. **False positive**. Occurs when the null hypothesis of no effect of exposure on disease is rejected for a given variant when in fact the null hypothesis is true.

1. **Family-wise error rate**. The probability of one or more false positives in a set of tests. For genetic association studies, family-wise error rates reflect false positive findings of associations between allele/genotype and disease.

1. **Fine mapping**: Extensively genotyping or sequencing a region of the genome that was identified in genome-wide association studies to identify the causal variant.

1. **Fixation**. The increase in the frequency of a genetic variant in a population to 100%.

1. **Flow cells**. Disposable parts of a next-generation sequencing routine. Template DNA is immobilized within the flow cell where fluid reagents can be streamed into the cell and flushed away. 

1. **Fluorescence in situ hybridization**. A technique in which fluorescently labelled DNA probes are hybridized to interphase cells, metaphase chromosome preparations or DNA fibres, as a means to determine the presence and relative location of target sequences.

1. **Fosmid**. A bacterially propagated phagemid vector system that is suitable for cloning genomic inserts of approximately 40 kb in size.

1. **Fosmid end sequence library**. Paired-end sequences from a collection of bacterial cloning vectors that can carry an average of 40 kb of DNA.

1. **FoSTeS/MMBIR (fork stalling and template switching/ microhomology- mediated break- induced replication)**: replication-based mechanisms of DNA repair that utilize nucleotide microhomology at the rearrangement breakpoints and can account for the formation of complex genomic rearrangements.

1. **Founder effect**: the loss of genetic diversity, the random fluctuation of allele frequencies, and the increase of linkage disequilibrium, that occur when a population develops from a small number of founders.

1. **Fragile site**. A small break or a constriction of a chromosome that can be visualized under special cell-culture conditions. Some fragile sites are universal, others are normal structural variants, and two are associated with mental retardation syndromes (FRAXA and FRAXE).

1. **Fragmentation**. The process of breaking large DNA fragments into smaller fragments. This can be achieved mechanically (by passing the DNA through a narrow passage), by sonication or enzymatically. 

1. **Frequency spectrum**. The distribution of allele frequencies at polymorphic sites, specified by the proportion of alleles in different frequency ranges.

1. **Frequentist**. A name for the school of statistical thought in which support for a hypothesis or parameter value is assessed using the probability of the observed data (or more ‘extreme’ datasets) given the hypothesis or value. Usually contrasted with Bayesian. 

1. **FST**. A statistical measure of population structure based on differences in variant frequencies between populations using genotypic data.

## G

1. **Gain-of-function alleles**: classically associated with dominant forms of monogenic disease, gain-of-function alleles are those variants that lead to a new function for the gene product.

1. **Gencode**. Integrated annotation of existing cDNA and protein resources to define transcripts with both manual review and experimental testing procedures 

1. **GENCODE annotation**. The GENCODE project produces high-quality reference gene annotation and experimental validation for human and mouse genomes. 

1. **Gene conversion**. A process in which one sequence directs the sequence conversion of a partner allele or paralogous sequence into its own form. | Recombination that involves the non-reciprocal transfer of information from one sister chromatid to another.

1. **Gene Ontology**. A project that provides sets of controlled vocabularies that have been developed to help describe and categorize genes. They describe the molecular function, biological process and cellular localization of gene products.

1. **Gene pathways**. Sets of interacting gene products that are related to particular functions, including signalling and metabolic pathways.

1. **Generalized additive model**. A statistical model that blends properties of generalized linear models with additive models (parametric or non-parametric) and is often used to estimate smoothing functions for scatter plots.

1. **Genetic architecture**: The underlying genetic basis of a trait or disease. The combination of the number, type, frequency, relationship between and magnitude of effect of genetic variants contributing to a trait.

1. **Genetic epidemiology** is the study of the causes, distribution and control of disease in groups of relatives and with inherited causes of disease in populations.

1. **Genetic Drift**:  The random fluctuations in allele frequencies over time that are due to chance alone.

1. **Genetic Load**. The proportion of a population’s maximum fitness that is lost as a result of selection against the deleterious genotypes it contains.

1. **Genetic map**. A genome map in which polymorphic loci are positioned relative to one another on the basis of the frequency with which they recombine during meiosis. The unit of distance is centimorgans (cM), denoting a 1% chance of recombination.

1. **Genome-wide association studies (GWAS)**: also known as whole-genome association studies (WGA), these involve the examination of genetic variation across a given genome using many hundreds of thousands of single nucleotide polymorphisms (SNPs) on DNA arrays. They are designed to identify genetic associations with observable traits and require large numbers of cases to identify the associated regions. Studies that search for a population association between a phenotype and a particular allele by screening loci (most commonly by genotyping SNPs) across the entire genome. 

1. **Genome-wide-significant**: A term describing the statistical significance threshold that accounts for multiple testing in GWASs.

1. **Genomic disorders**. A group of human diseases that are caused by recurrent genomic rearrangements of unstable genomic regions. These give rise to phenotypes as a result of abnormal gene dosage within the rearranged genomic region. Segmental duplications are often involved in the rearrangement event.

1. **Genotype imputation**: Inference of missing genotypes or untyped single-nucleotide polymorphisms using statistical techniques.

1. **Genotypic relative risks (GRR)** is the increased chance that an individual with a particular genotype has the disease.

1. **Genome structure correction (GSC)**: a method to adapt statistical tests to make fewer assumptions about the distribution of features on the genome sequence. This provides a conservative correction to standard tests.

1. **gRNA scaffolds**. The backbone (invariable) portions of guide RNAs, which are recognized by Cas proteins.


## H

1. **Haploinsufficiency**. This occurs when a diploid organism only has one copy of a gene and both copies are required for correct function. This is one way that a protein-truncating mutation can influence predisposition to a disease.

1. **Haplotype**. A combination of alleles at multiple linked sites on a single chromosome, all of which are transmitted together. / A set of alleles on a chromosome or chromosomal segment inherited from one parent — often a series of alleles at neighbouring loci that are strongly statistically associated due to lack of recombination. Certain haplotypes may become common in the population owing to natural selection or drift until broken down over time by recombination.

1. **Haplotype block**, a region containing strongly associated SNPs.

1. **Helicase**. An enzyme that separates the two nucleic acid strands of a double helix, resulting in the formation of regions of ssDNA or ssrNA.

1. **Heritability**. The proportion of phenotypic variation in a trait that is due to underlying genetic variation. In studies of humans, this value is usually calculated by comparing trait correlations in individuals of varying degrees of relatedness. The proportion of total variation between individuals within a population that is due to genetic factors.

1. **Heterochromatin**. A highly condensed form of chromatin (the eukaryotic complex of DNA with proteins) that shows reduced gene expression and is replicated late in s phase.

1. **Heteromorphism**: A microscopically visible region of a chromosome that varies in size, morphology or staining properties. They include euchromatic and non-euchromatic variation, such as satellite–satellite stalk variation and heterochromatic variation (centromeres and other C-band positive regions).

1. **Hierarchical clustering**. A statistical method in which objects (for example, gene expression profiles for different individuals or tissue samples) are grouped into a hierarchy, which is visualized in a dendrogram. Objects close to each other in the hierarchy, as measured by tracing the branch heights, are also close by some measure of distance — for example, between gene expression profiles. Individuals or samples with similar expression profiles will be close together in terms of branch lengths. 

1. **Hitchhiking**: the process by which the frequency of neutral or weakly selected mutations linked to an advantageous mutation are influenced by the spread of the advantageous mutation through a population. The observed patterns is of neutral variants linked to new, positively selected mutations.

1. **Hidden Markov model (HMM)**: a machine-learning technique that can establish optimal parameters for a given model to explain the observed data.

1. **Holliday junction**. A point at which the strands of two dsDNA molecules exchange partners. This structure occurs as an intermediate in crossing over.

1. **Hominin**. All the taxa on the human lineage after the split from the common ancestor with the chimpanzee.

1. **Homologues**. A pair of genes that descended from a common ancestral gene. 

1. **Homopolymer**. A sequence run of identical bases. 

1. **Homozygosity mapping**. Narrowing down the location of a gene underlying a trait by searching for regions of the genome in which both chromosomal segments are inherited identically- by-descent. 

1. **Horizontal Transfer**. The transfer of genetic material between members of the same generation, or between members of different species.

1. **Human Genome Diversity Panel (HGDP)**, a panel of cell lines established at the CEPH (http://www.cephb.fr/HGDP-CEPH-Panel/), comprising 1064 lymphoblastoid cell lines from 54 populations of the world.

1. **Human genome reference assembly**. The standard reference DNA sequence (or assembly) of the human genome that is regularly curated (successive updates named ‘builds’). The assembly is derived mostly (>60%) of DNA from a bacterial artificial chromosome (BAC) library made from a single donor, with the rest of the sequence originating from a mosaic of other sources. The current assembly covers most of the euchromatic regions of the human genome, but there are still some gaps remaining, and many of these co-locate with segmental duplications and/or CNVs.

1. **Human mutation rate**. The rate (per base pair) at which mutations appear in the genome sequence of an individual at each generation or year. Currently, the exact value of this rate in humans is a topic of debate, with most estimates ranging from a value of 0.5×10−9 per base pair per year to a value of 10−9 per base pair per year. 

## I

1. **Identity-by-descent**. Alleles on different chromosomes that are identical because they are inherited from a shared common ancestor. / The inheritance of an identical haplotype from both parents owing to it having been passed without recombination from a common ancestor in the baseline population.

1. **Identity-by-state**.  Alleles on different chromosomes that are identical but do not share a common ancestor with respect to a pedigree or population of interest. 

1. **Imprinted gene**. A gene for which expression is determined by the parent that contributed it.

1. **Imputation**. Based on the known linkage disequilibrium structure in fully genotyped individuals, the genotype of untyped variants can be inferred in individuals who are genotyped for a smaller number of variants. | A technique whereby a genomic variant is not directly genotyped, but instead its presence or absence is inferred on the basis of its association (co-occurrence) with another, genotyped variant. Otherwise: the prediction of missing genotype data based on genotype at physically proximal tested SNPs.

1. **Imputation reference panel**: A data set containing genetic information on a large number of individuals who have undergone whole-genome sequencing and had their haplotypes reconstructed. These haplotype panels enable accurate imputation of non-genotyped genetic variants in individuals who have undergone genome-wide genotyping.

1. **Incidence**: is the rate of new (or newly diagnosed) cases of the disease. It is generally reported as the number of new cases occurring within a period of time (e.g., per month, per year). It is more meaningful when the incidence rate is reported as a fraction of the population at risk of developing the disease (e.g., per 100,000 or per million population). 

1. **Incidental findings**. Findings that are not explicitly related to the original research hypotheses (that is, primary findings. 

1. **Infinite site model**. This assumes that each new mutation occurs at a site that has not previously mutated. It is a good approximation when mutation rates at all sites are relatively low.

1. **International HapMap Project**. A public available genome-wide data set of common single nucleotide polymorphisms generated from global populations.

1. **Inbreeding coefficient**: The probability, denoted F, of inheriting two alleles identical-by‑descent at an autosomal locus in the presence of consanguinity. F is one-sixteenth for first-cousin offspring, one-sixty-fourth for second cousins and one-eighth for the progeny of avuncular or double first-cousin matings.

1. **Incomplete lineage sorting**, that is, the random assortment of genetic lineages due to genetic drift which may have allowed a divergent mtDNA lineage to survive in a species by chance while becoming lost in other sister species.

1. **Inclusive fitness**: The sum of the reproductive fitness of an individual and the indirect fitness received by relatives other than the individual’s own offspring that were produced as a result of help from the individual.

1. **Insertion/deletion (‘indel’)**. Indel is a collective abbreviation to describe relative gain or loss of a segment of one or more nucleotides in a genomic sequence. It allows the designation of a difference between genomes in situations where the direction of sequence change cannot be inferred: for example, when a reference or ancestral sequence has not been defined. It has typically been used to denote relatively small-scale variants (particularly those smaller than 1 kb).

1. **Insulator**: is a genetic boundary element that blocks the interaction between enhancers and promoters. Insulators determine the set of genes an enhancer can influence. The need for them arises where two adjacent genes on a chromosome have very different transcription patterns, and it is critical that the inducing or repressing mechanisms of one do not interfere with the neighbouring gene

1. **Isochromosome**. A chromosome that has two genetically and morphologically identical arms.

1. **Isolation by distance**. A model in which the amount of gene flow between two locations decreases as a function of distance. At equilibrium, this model predicts that genetic differentiation increases as a function of geographic distance. Sometimes the term refers simply to this emergent pattern, rather than the model. 

## K

1. **K-mer**. A substring within a sequence of bases of some (k) length. Currently, k-mer sizes of Oxford Nanopore Technologies (ONT) range from 3 to 6 bases. 

1. **Kin selection**: A form of natural selection that favours the reproductive success of relatives even at a cost to an individual’s own survival and reproduction.

1. **Kurtosis**. The peakedness of the distribution.

## L

1. **Leptokurtic**. Used to refer to distributions that are more peaked than an exponential distribution.

1. **Library**. The collection of processed genome fragments that are prepared for sequencing. In a bioinformatics context, the term may also generally refer to the set of sequences found in a single fastq file. 

1. **Likelihood**. A statistical model for analysing data that requires specifying a particular form for the distribution of the data.

1. **Likelihood-ratio test**. A statistical test that is based on the ratio of likelihoods under alternative and null hypotheses. If the null hypothesis is a special case of the alternative hypothesis, then the likelihood-ratio statistic typically has a É‘2 distribution with degrees of freedom equal to the number of additional parameters under the alternative hypothesis. 

1. **LINE**. Long interspersed nuclear elements. A class of transposable element lacking long terminal repeats.

1. **Linear mixed-effect model**: A linear model that contains both fixed and random effects. This type of model can be used to estimate genetic correlation between traits using a genome-wide set of single-nucleotide polymorphisms.

1. **Link (or edge)**. A link represents the interactions between the nodes of a network. In biological systems, interactions can correspond to protein–protein binding interactions or metabolic coupling, or they may represent connections between diseases based on a common genetic origin or shared phenotypic characteristics. 

1. **Linkage analysis**: it is a genetic technique used to map genetic loci by use of observations of related individuals. Linkage extends over much longer regions of the genome than does linkage disequilibrium. Otherwise: Linkage analysis is the statistical analysis of genetic data with the goals of detecting whether θ < 1/2 , of estimating θ, of ordering a set of genetic loci, and ultimately of placing the loci determining genetic traits of interest at correct locations in a genetic map. 

1. **Linkage disequilibrium**: the non-random association of alleles at two or more loci, not necessarily on the same chromosome. It is not the same as genetic linkage, which describes the tendency of certain loci or alleles to be inherited together. Genetic loci on the same chromosome are physically close to one another and tend to stay together during meiosis, and are thus genetically linked. In general, two loci in linkage disequilibrium will also be linked, but the reverse is not necessarily true. 

1. **Locus heterogeneity**. Refers to the number of different genes in the genome that can carry mutations that influence risk of given disease (see Allelic heterogeneity). The appearance of phenotypically similar characteristics resulting from mutations at different genetic loci. Differences in effect size or in replication between studies and samples are often ascribed to different loci leading to the same disease. 

1. **LOD** score (logarithm (base 10) of odds). By convention, a LOD score greater than 3.0 is considered evidence for linkage, as it indicates 1000 to 1 odds that the linkage being observed did not occur by chance. 

1. **Log-linear model**: A statistical model that captures the dependence among a set of categorical variables.

1. **Long non-coding RNAs**. (lncRNAs). Non-protein coding transcripts that are longer than 200 nucleotides. This somewhat arbitrary limit distinguishes lncRNAs from small regulatory RNAs. 

1. **Longitudinal study**. An observational study in which individuals are followed for a long period of time, often many decades, and in which the same traits are measured repeatedly.

1. **Loss-of-function alleles**: sometimes also called null alleles and most classically associated with recessive forms of disease, loss-of-function alleles are those alleles that result in a gene product having less and sometimes no function. Perhaps the most easily recognized loss-of-function alleles are those that clearly disrupt the production of a protein product, for example protein- truncating mutations caused by frameshift or premature stop mutations.

1. **Loss of heterozygosity**. (LOH).Commonly used in the context of oncology, refers specifically to the loss of function of an allele, when a second allele is already inactive, through inheritance of the heterozygous genotype.

## M

1. **Marker chromosome**. (Also known as an extra-structurally abnormal chromosome or ‘supernumerary’ chromosome.) Chromosomes that are seen in addition to the normal chromosome complement in fluorescence in situ hybridization experiments.

1. **Markov chain Monte Carlo technique**: A simulation technique for producing samples from an unknown probability distribution. By evaluating the probability of the observed data at each step in the Markov chain, an estimate of the probability distribution of model parameters can be obtained by observing the behaviour of the chain as it proceeds through many steps.

1. **Maximum-likelihood estimate**. The value of an unknown parameter that maximizes the probability of the observed data under the assumed statistical model. 

1. **Mendelian disease**. A disease that is carried in families in either a dominant or recessive manner and that is typically controlled by variants of large effect in a single gene.

1. **Metabolomic**. Pertaining to the metabolome, which is the combined set of metabolites that are present in a given tissue at a given time.

1. **Microsatellite**: a short, highly repetitive DNA sequence of 2–6 base pairs occurring as a repetition of di-, tri-, or tetranucleotides. In the genome, microsatellites are widely spread and usually lie in noncoding regions.

1. **MicroRNAs** (miRNAs). Derived from primary transcripts with features similar to mRNAs that are enzymatically processed to their mature length of 21–24 nucleotides by Drosha and Dicer enzymes. 

1. **Minimum number of recombination events (RM)**. Under the infinite site model, a recombination event between a pair of polymorphic sites can be inferred if all four haplotypes are observed. RM is the maximum number of (non-overlapping) such pairs.

1. **Minor Allele**. The less frequent of two alleles at a locus.

1. **Minor allele frequency**. Ranging from 0 to 50%, this is the proportion of alleles at a locus that consists of the less frequent allele. This number does not take genotype into account. In other words, it is the proportion of chromosomes in the population carrying the less common variant.

1. **Minor-allele frequency versus altered copy-number frequency**. The minor allele is the less common allele at a polymorphic locus. The use of this term is complicated when a locus is multiallelic. Locke et al. proposed use of altered copy-number frequency because measurements of copy number are on diploid samples and screening methods do not necessarily distinguish the two independent alleles. Redon et al. adopted the convention of assuming that the minor allele is the derived allele; thus, deletions have a minor allele of lower copy number and duplications have a minor allele of higher copy number.

1. **Mismatch repair**. A DNA repair system that corrects a mismatched base pair in duplex DNA by excision of a length of one strand followed by synthesis of the sequence complementary to the remaining strand.

1. **Modern humans**: Present-day humans and their recent ancestors, up to the time at which they diverged from their most closely related archaic human groups, the Neanderthals and Denisovans. 

1. **Module (or community)**. A dense subgraph on the network that often represents a set of nodes that have a joint role. In biology, a module could correspond to a group of molecules that interact with each other to achieve some common function. 

1. **Monogenic**: A term used to describe diseases with one contributing gene, that is, familial risk is driven by high-risk variants, which is in contrast to polygenic disease, where several genetic factors contribute to the disease.

1. **Morbidity**: is a diseased state, disability, or poor health due to any cause. The term may be used to refer to the existence of any form of disease, or to the degree that the health condition affects the patient. In epidemiology, the term "morbidity rate" can refer to either the incidence rate, or the prevalence of a disease or medical condition. This measure of sickness is contrasted with the mortality rate of a condition, which is the proportion of people dying during a given time interval.

1. **mtDNA**. Mitochondiral DNA.

1. **Muller’s ratchet**. The process by which a genome with little or no recombination degenerates owing to the stochastic loss of the allelic class with fewest deleterious mutations.

1. **Multi-modal**. A distribution with more than one peak or mode.

1. **Multidimensional scaling (MDS)**. A technique used to display the information contained in a distance matrix. It aims to place each object in N-dimensional space such that the between-object distances are preserved as well as possible. 

1. **Multiplicative genetic model**. A genetic model for penetrance functions that assumes the relative risk for disease given two alleles is the square of the relative risk for disease given only one allele. 

1. **Multiple-hypothesis testing**. Many different statistical tests are used on the same sample; for example, many genetic markers might be tested against many different phenotypes. Failure to account for multiple testing inflates the study-wide type-1error rate. 

## N

1. **N50 length** A measure of the contig length (or scaffold length) containing a `typical' nucleotide. Specifically, it is the maximum length L such that 50% of all nucleotides lie in contigs (or scaffolds) of size at least L.

1. **Nano-channel flow cells**. Specialized flow cells narrow enough for a single DNA molecule to pass through in linear form without having sufficient room to fold over on itself.

1. **Nanoslits**. Narrow channels (~1μm wide) on specialized silicon substrates. They are loaded with linear stretched DNA strands by applying a charge to microchannels on the substrates that contain electrodes.

1. **Narrow-sense heritability**. The proportion of phenotypic variation that can be accounted for by additive genetic effects. By contrast, broad-sense heritability includes effects of interactions among genes that are caused by dominance and epistasis.

1. **Negative selection**: selection acting upon new deleterious mutation.

1. **Neolithic**. A human cultural period, beginning approximately 10,000 years ago, marked by the appearance in the archaeological record of industries such as polished stone and metal tools, pottery, animal domestication and agriculture.

1. **Neutral mutation**: a mutation that does not affect the fitness of individuals who carry it in either heterozygous or homozygous condition

1. **Neutral theory**. This posits that the vast majority of polymorphisms within species and fixed substitutions between species are the result of the random drift of neutral mutations, rather than of natural selection. Deleterious mutations are also assumed to occur, but are quickly eliminated.

1. **Neutrality test**: a statistical test of a model which assumes all mutations are either neutral or strongly deleterious.

1. **Nested models**. A sequence of statistical models, each specifying a different hypothesis, such that each model in the sequence contains one more factor than the preceeding model. Nested models are often used to test for the presence of interactions between two or more risk factors.

1. **Next-generation DNA sequencing**. Highly parallelized DNA-sequencing technologies that produce many hundreds of thousands or millions of short reads (25–500 bp) for a low cost and in a short time. 

1. **Node (or vertex)**. A system component that, by interacting with other components, forms a network. In biological networks, nodes can denote proteins, genes, metabolites, RNA molecules or even diseases and phenotypes. 

1. **Non-allelic homologous recombination**. Homologous recombination between lengths of homology in different genomic positions.

1. **Non-parametric approach**: A statistical analysis method that does not rely on specific distributional assumptions (for example, normality) for the variables being analysed.

1. **Nonmendelian inheritance** (also called mendelian incompatibilities or mendelian inconsistencies). These terms refers to transmission from parent(s) to offspring in a manner that does not conform to expectations of classical allelic segregation. (Avoid the term ‘mendelian errors’.) Evidence in family studies (‘trios’ in the HapMap data) of apparent nonmendelian inheritance for a genomic segment indicates that copy number variation may be involved.

1. **NRY**: nonrecombining portion of the Y chromosome.

1. **Non-synonymous variant**. A genetic variant that changes a codon for one amino acid to another amino acid. Many non-synonymous variants are well-tolerated, but others can cause a disease.

1. **Non-synonymous SNP**. A SNP for which each allele codes for a different amino acid in the protein sequence. 

1. **Nucleotide diversity per site** (PI). This is the frequency with which any two sequences in the sample differ at a site. Under the standard neutral model, the means of π and w are equal; their values will vary in accord with the expectation that, for example, higher mutation rates and/or larger population size will lead to higher polymorphism levels.

1. **Number of polymorphic sites in the sample (S)**. S depends on the sample size. Therefore, comparing S across surveys requires a sample size correction. Under the standard neutral model, (here equation E(S)= ... where E(S) denotes the expectation of S and n is the sample size. This equation leads to a commonly used estimate of  based on S, denoted  w.

## O

1. **Odds**. The chances or likelihood of something happening. 

1. **Odds ratio**. A measure of association derived from case-control studies. The ratio of the odds of an event occurring in one group to the odds of it occurring in another group; can be presented  in terms of increased risk per copy of the variant (allelic OR), of a heterozygous carrier compared with a noncarrier (heterozygote OR), or of individuals with the heterozygous and homozygous variant genotypes separately compared with those homozygous for the nonrisk allele (genotypic OR). - **LOD** score (logarithm (base 10) of odds), 

1. **Okazaki fragment**. The discontinuous length of DNA that is synthesized as one piece on the lagging strand template during DNA replication.

1. **Optical mapping**. A technology that uses in situ restriction digests of individual DNA molecules from genomic DNA to produce detailed optical-restriction maps of genomes.

1. **Orthologous**. Homologous genes in different species that have evolved from a common ancestral gene by speciation. 

1. **Outgroup**. A related species that is used for comparison, for example, to infer the ancestral versus the derived state of a polymorphism.

## P

1. **Paired-end reads**. Two reads sequenced from the start and end of the same molecule (such as a fosmid, bacterial artificial chromosome or next-generation sequence fragment).

1. **Paired-end DNA sequencing**. A method whereby DNA is fragmented and sequenced from both ends. If the DNA fragments are size-selected, discrepancies in the distance and/or orientation of the two ends compared with a reference genome can reveal the presence of deletions, duplications, inversions and translocations.

1. **Pairwise linkake disequilibrium**. (Pairwise LD). The strength of association between alleles at two different markers.

1. **Parametric tests**. Statistical significance tests for which P values are based on models or assumed formulae for the distribution of the test statistic. 

1. **Paraphyletic**: When the common ancestor of one natural group is shared with any other such group.

1. **Parthenogenetic**: Pertaining to parthenogenesis, which is a form of asexual reproduction that produces viable embryos from eggs without fertilization by sperm — notably, haploid male production in Hymenoptera. In some parthenogenetic insects such as Cerapachys biroi, female reproductives can lay diploid eggs by thelytoky, thereby producing clonal female offspring.

1. **Patch**: A genome patch is a scaffold sequence that is part of a minor genome release. These sequences either correct errors in the assembly (a FIX patch) or add additional alternate loci (a NOVEL patch). These sequences allow us to update the assembly information without disrupting the chromosome coordinate system. FIX patches will be removed at the next major assembly release, as the changes will be rolled into the new assembly. NOVEL patches will be moved from the PATCHES assembly unit to a proper assembly unit. 

1. **Penetrance**. The proportion of genotypically mutant organisms that show the mutant phenotype. If all genotypically mutant individuals show the mutant phenotype, then the genotype is said to be completely penetrant. 

1. **Permutation Test**. An approach in which the actual data are randomized many times to generate a distribution of outcomes, so that the fraction of observations with values that are more extreme than the outcome that is observed with the real data reflects the statistical significance.

1. **PET**: A short sequence that contains both the 5' and 3' ends of a transcript

1. **Phenotype**: A measurable characteristic of an individual.

1. **PHRAP**: A widely used computer program that assembles raw sequence into sequence contigs and assigns to each position in the sequence an associated `quality score', on the basis of the PHRED scores of the raw sequence reads. A PHRAP quality score of X corresponds to an error probability of approximately 10- X/10. Thus, a PHRAP quality score of 30 corresponds to 99.9% accuracy for a base in the assembled sequence.

1. **PHRED**: A widely used computer program that analyses raw sequence to produce a `base call' with an associated `quality score' for each position in the sequence. A PHRED quality score of X corresponds to an error probability of approximately 10- X/10. Thus, a PHRED quality score of 30 corresponds to 99.9% accuracy for the base call in the raw read. 

1. **Pleiotropy**: The action of a single gene on two or more distinct phenotypic characters.

1. **Pleistocene**. An epoch of the Quaternary period beginning 1.8 million years ago and transitioning to the Holocene epoch approximately 10,000 years ago. The Pleistocene is characterized by a cool climate and extensive glaciation of northern latitudes.

1. **Polar Body**. A small haploid cell that is produced during oogenesis and that does not develop into a functional ovum. 

1. **Polygenic Risk Scores** (PRSs). A weighted sum of the number of risk alleles carried by an individual, where the risk alleles and their weights are defined by the loci and their measured effects as detected by genome wide association studies. 

1. **Polymorphism**. The contemporary definition refers to any site in the DNA sequence that is present in the population in more than one state. By contrast, the traditional definition referred to an allele with a population frequency >1% and <99%.

1. **Polymorphism Data**. Data that include the genotypes of many individuals sampled at one or more loci; we consider a locus to be polymorphic if two or more distinct types are observed, regardless of their frequencies.

1. **Population mutation parameter**. This term denotes 4Ne(or 3Ne if X linked), where is the neutral mutation rate per generation. This can be estimated from the number of nucleotide differences fixed between two species (or divergence), given an estimate of the time to the common ancestor.

1. **Population recombination parameter (C )**. Similarly, C = 4Nec where c is the recombination rate per generation.

1. **Population stratification**: is the presence of a systematic difference in allele frequencies between subpopulations in a population possibly due to different ancestry, especially in the context of association studies. Population stratification is also referred to as population structure. A source of bias in genome-wide association studies that occurs when a phenotype and the allele frequency of a single-nucleotide polymorphism vary owing to ancestral differences.

1. **Population Structure**. A departure from random mating as a consequence of factors such as inbreeding, overlapping generations, finite population size and geographical subdivision.

1. **Position effect**. A change in the expression of a gene that is produced by changing its location within a genome.

1. **Positive selection**: selection acting upon new advantageous mutations.

1. **Posterior probability**. The probability of an event after combining prior knowledge of the event with the likelihood of that event given by observed data. 

1. **Power**. The probability of correctly rejecting the null hypothesis when it is truly false. For association studies, the power can be considered as the probability of correctly detecting a genuine association. 

1. **Pre-ascertained SNPs**. SNPs that have already been detected in previous studies, usually from an extremely small sample of chromosomes.

1. **Precision medicine**. An emerging approach for disease treatment and prevention that takes into account individual variability in genes, environment and lifestyle for each person. 

1. **Predictive biomarkers**: Measurable biological entities or phenotypes that are associated with response to a specific therapy.

1. **Prevalence** is the actual number of cases alive, with the disease either during a period of time (period prevalence) or at a particular date in time (point prevalence). Prevalence is also most meaningfully reported as the number of cases as a fraction of the total population at risk and can be further categorized according to different subsets of the population.

1. **Primary Assembly Unit**: Represents the collection of sequences that, when combined, represent a non-redundant haploid genome. 

1. **Principal-components analysis (PCA)**. A statistical technique for summarizing many variables with minimal loss of information: the first principal component is the linear combination of the observed variables with the greatest variance; subsequent components maximize the variance subject to being uncorrelated with the preceding components. 

1. **Proband**. In a family study, this is the individual who is first identified in the family as having the disease under study. 

1. **Processed pseudogenes**. Copies of the coding sequences of genes that lack promoters and introns, contain poly(A) tails and are flanked by target-site duplications. 

1. **Pronucleus**. The haploid nucleus of an egg or sperm. 

1. **Prospective study design**. Studies in which individuals are followed forward in time and disease events are recorded as they arise. DNA and biomarker samples, and data on environmental exposures and lifestyle factors, are usually obtained at the start of the study. (See retrospective studies).

1. **Pseudogenes**. Segments of DNA that originate from functional genes, but have lost at least some of the ability of the parent gene in terms of expression or coding potential. 

1. **Purifying selection**. Selection against a functionally deleterious allele. 

## Q

1. **Quadratic regression**. A quadratic regression estimates the parameters of an equation for a parabola that best fits the data. Here it is incorporated in a larger model that also has linear elements.

1. **Quantitative trait loci (QTL)**: a region in the genome where genetic variation is correlated with variation of a quantitative trait of interest.

## R

1. **r2**. LD coefficient, representing the proportion of observations in which two specific pairs of alleles occur together.

1. **RACE Rapid amplification of cDNA ends**: a technique for amplifying cDNA sequences between a known internal position in a transcript and its 5’ end.

1. **Rare variant**: A genetic variant with a minor-allele frequency of less than 1%. Rare variants are typically single-nucleotide substitutions but can also be structural variants.

1. **Read**. The sequence of bases from a single molecule of DNA. 

1. **Real-time sequencing**. A sequencing strategy used in the Pacific Biosciences (PacBio) and Oxford Nanopore Technologies (ONT) platforms. In these approaches there is no pause after the detection of a base or series of bases, thus the sequence is derived in real-time. 

1. **Recombination Hotspot**: Short regions (typically spanning about 2kb) over which recombination rates rise dramatically over local background rates.

1. **Regression models**. A class of statistical models that relate an outcome variable to one or more explanatory variables. The goal might be to predict further values of the outcome variable given the explanatory variables, or to identify a minimal set of explanatory variables with good predictive power.

1. **Relative Risk (RR):** The relative risk (RR) is defined as the probability that an individual exposed to the risk factor develops the disease (e.g., becomes a case) divided by the probability that an unexposed individual develops the disease. RR= [Pr(case|exposed)/Pr(case|unexposed)]. Relative risk is the probability, or risk, that a certain event will occur in comparison to the event rate in a reference group; often expressed as the ratio of absolute risk between two groups, thus a value of 1.0 means no difference in risk.

1. **Reproductive Risk**. The risk of establishing a pregnancy in which a fetus miscarries or has a phenotypic abnormality as a consequence of the familial genetic condition. 

1. **Retrospective study design**. Studies in which individuals are identified for inclusion in the study on the basis of their disease state. Data on previous environmental exposures and lifestyle factors are then recorded, and samples for DNA and biomarker studies might be obtained. 

1. **Retrotransposon**. A transposon (mobile element) that is copied from the host genome by transcription as rNA, and is later reverse-transcribed into DNA and reintegrated into the host genome.

1. **RFBR Regulatory factor binding region**: a genomic region found by a ChIP-chip assay to be bound by a protein factor.

1. **RFBR-Seq** Regulatory factor binding regions that are from sequence-specific binding factors.

1. **Rolling circle amplification (RCA)**. A method of DNA amplification using a circular template. Briefly, DNA polymerase binds to a primed section of a circular DNA template. As the polymerase traverses the template, a new strand is synthesized. When the polymerase completes a full circle and encounters the double-stranded DNA (dsDNA) template, it displaces the template without degradation, thus creating a long ssDNA fragment composed of many copies of the template sequence. 

1. **RNA-PET**. Simultaneous capture of RNAs with both a 5' methyl cap and a poly(A) tail, which is indicative of a full-length RNA. This is then followed by sequencing a short tag from each end by high-throughput sequencing.

1. **RNA-seq**. Isolation of RNA sequences, often with different purification techniques to isolate different fractions of RNA followed by high- throughput sequencing.

1. **RRBS. Reduced representation bisulphite sequencing**. Bisulphite treatment of DNA sequence converts unmethylated cytosines to uracil. To focus the assay and save costs, specific restriction enzymes that cut around CpG dinucleotides can reduce the genome to a portion specifically enriched in CpGs. This enriched sample is then sequenced to determine the methylation status of individual cytosines quantitatively.

1. **RT–PCR Reverse transcriptase polymerase chain reaction**: a technique for amplifying a specific region of a transcript.

1. **RxFrag Fragment of a RACE reaction**: a genomic region found to be present in a RACE product by an unbiased tiling-array assay.

## S

1. **Sanger sequencing**. An approach in which dye-labelled normal deoxynucleotides (dNTPs) and dideoxy-modified dNTPs are mixed. A standard PCR reaction is carried out and, as elongation occurs, some strands incorporate a dideoxy-dNTP, thus terminating elongation. The strands are then separated on a gel and the terminal base label of each strand is identified by laser excitation and spectral emission analysis. 

1. **Scaffold**. The result of connecting contigs by linking information from paired-end reads from plasmids, paired-end reads from BACs, known messenger RNAs or other sources. The contigs in a scaffold are ordered and oriented with respect to one another.

1. **Secondary constriction**. A thin chromatic filament that connects a chromosomal satellite with the rest of the chromosome.

1. **Sequencing depth** The total amount of raw sequence mapped to a reference genome, divided by the length of the genome. 

1. **Sequencing coverage**. In a sequencing experiment, the number of reads covering a specific nucleotide position is the coverage of that position. Increasing read depth leads to increasing coverage, and to increasing accuracy of the base calls. 

1. **Segmental duplication** (also called low-copy repeat (LCR) or duplicon). A segment of DNA >1 kb in size that occurs in two or more copies per haploid genome, with the different copies sharing >90% sequence identity. These segments can also be CNVs. The duplicated blocks predispose to nonallelic homologous recombination.

1. **Segmental uniparental disomy**. Uniparental disomy (often abbreviated UPD) is a cryptic alteration in which two copies of a chromosome or segment (segmental UPD) are present, but derive from a single parent.

1. **Selection differential**. The average superiority of the selected parents; it is expressed as the mean phenotypic value of the individuals selected as parents and expressed as a deviation from the population mean.

1. **Selective constraint**: the selective constraint on a site, or the average constraint across multiple sites, is a function of the degree to which fitness is reduced when mutated. In the absence of positive selection, pN/pS and dN/dS provide measures of the selective constraint on a site because they measure the extent to which the rate of nonsynonymous variation is reduced by negative selection relative to synonymous variation (assuming synonymous variation is neutral).

1. **Selective sweep**: the process by which a new advantageous mutation eliminates or reduces variation in linked neutral sites as it increases in frequency in the population. The process by which positive selection for a mutation eliminates neutral variation at linked sites.

1. **Sensitivity** (also called the true positive rate) measures the proportion of actual  positives which are correctly identified as such (e.g., the percentage of sick people who are correctly identified as having the condition), and is complementary to the false negative rate.

1. **Sequence-tagged site (STS)** is a short (200 to 500 base pair) DNA sequence that has a single occurrence in the genome and whose location and base sequence are known (a polymerase chain reaction assay has been developed).

1. **Significance level**. Usually denoted Éø, and chosen by the researcher to be the greatest probability of type-1 error that is tolerated for a statistical test. It is conventional to choose Éø = 5% for the overall analysis, which might consist of many tests each with a much lower significance level. 

1. **SINE**. Short interspersed nuclear elements. A class of short (<500 bp) transposable elements.

1. **Single-base extension**. Single-base-extension reactions use a primer that binds to a region of interest and follow this with an extension reaction that allows the incorporation of a single base after the primer.

1. **Single-strand annealing**. A double-stranded break repair mechanism that deletes sequence between repeats.

1. **Site-frequency spectra (SFS)**. The frequency spectrum is a count of the number of mutations that exist at a frequency of xi = i/n for i = 1, 2,…, n−1, in a sample of size n. In other words, it represents a summary of the allele frequencies of the various mutations in the sample. | The fraction of polymorphic sites at which a minor or derived allele is present in one copy, two copies and so on. In a standard neutral model (i.e., a model with random mating, constant population size, no population subdivision, etc.), the expected value of xi is proportional to 1/i.

1. **Small nuclear RNAs and small nucleolar RNAs (snRNAs and snoRNAs)**. Classes of short non-coding RNAs (100–200 nucleotides) that have important regulatory roles in nuclear ribonucleoprotein complexes. 

1. **SNP** Single nucleotide polymorphism: a single base pair change between two individuals in the human population.

1. **SNP microarrays**. Hybridization-based assays in which the target DNA sequences are discriminated on the basis of a single base difference. Assays are processed with a single sample per array and perform both SNP genotyping and copy-number interrogation.

1. **SNP tagging**. ‘Tagging’ refers to methods to select a minimal number of SNPs that retain as much as possible of the genetic variation of the full SNP set. 

1. **Spacer**.  The interchangeable portion of the guide RNA that is complementary to the targeted sequence.

1. **Specificity** (also called the true negative rate) measures the proportion of negatives which are correctly identified as such (e.g., the percentage of healthy people who are correctly identified as not having the condition), and is complementary to the false positive rate.

1. **Splice-site variant**. A variant, usually found at the intron–exon boundary, that alters the splicing of an exon to its surrounding exons.

1. **Stabilizing selection**. Natural selection that favours intermediate values of a quantitative trait.

1. **STAGE Sequence tag analysis of genomic enrichment**: a method similar to ChIP-chip for detecting protein factor binding regions but using extensive short sequence determination rather than genomic tiling arrays.

1. **Standard neutral model**. A hypothetical panmictic (randomly mating) population of constant size in which genetic variation is neutral and follows a model (the ‘infinite sites model’) in which each new mutation occurs at a site that has not previously mutated.

1. **Stillbirth**: the birth of an infant that has died in the womb (strictly, after having survived through at least the first 28 weeks of pregnancy, earlier instances being regarded as abortion or miscarriage).

1. **Stochastic Process**. A mathematical description of the random evolution of a quantity through time.

1. **Structural variant**. It is the umbrella term to encompass a group of genomic alterations involving segments of DNA typically larger than 1 kb, and which can be microscopic or submicroscopic. We use the term as a neutral descriptor with nothing implied about frequency, association with disease or phenotype, or lack thereof. This definition of size, though perhaps somewhat arbitrary, was undertaken to accommodate this significant class of variation that spans the gap between small variants (such as variable number of tandem repeats (VNTRs)) detected with molecular genetic assays and those recognized microscopically on karyotypes. The structural variation may be quantitative (copy number variants comprising deletions, insertions and duplications) and/or positional (translocations) or orientational (inversions).

1. **Summary statistics**: A statistic that summarizes a set of observations. In the context of genome-wide association studies, meta-analyses can be carried out solely by using summary statistics and typically include estimates of the effect size (for example, odds ratio) and standard error.

1. **Support vector machine (SVM)**: a machine-learning technique that can establish an optimal classifier on the basis of labelled training data.

1. **Syndrome**:  is the association of several medical signs, symptoms, and or other characteristics that often occur together. Some syndromes, such as Down syndrome, have only one cause; others, such as Parkinsonian syndrome, have multiple possible causes. In other cases, the cause of the syndrome is unknown. A familiar syndrome name often remains in use even after an underlying cause has been found, or when there are a number of different possible primary causes.

1. **Synteny**. Shared genomic organization between related species. It is usually seen as a shared relative order of genes or other functional elements on a portion of a chromosome.

## T

1. **Tag Cluster**: The genomic regions in which two or more tags (of 20 nucleotides in length) overlap each other (both being mapped to the same strand).

1. **Tag library**. A tag library is similar to a conventional cDNA library, except that, subsequently to isolation and cloning of the cDNA, small fragments are generated by restriction- enzyme cleavage, concatamerized and recloned. This approach enables efficient DNA sequencing of thousands of tags from a single library.

1. **Tag SNP**. A representative SNP in a region of the genome with high LD to other variants. Single-nucleotide polymorphisms (SNPs) chosen to represent a region of the genome owing to strong linkage disequilibrium.

1. **Tajima’s D** . A statistic used to test the standard neutral model for a given region of DNA sequence. It is the standardised difference between the number of pairwise nucleotide differences and the total number of segregating sites.

1. **Telomere**. A structure at the ends of linear chromosomes that avoids shortening of chromosomes after replication, and that protects the end from homologous and non-homologous recombination.

1. **Template**. A DNA fragment to be sequenced. The DNA is typically ligated to one or more adapter sequences where DNA sequencing will be initiated. 

1. **Test statistic**. A numerical summary of the data that is used to measure support for the null hypothesis. Either the test statistic has a known probability distribution (such as É‘2) under the null hypothesis, or its null distribution is approximated computationally. 

1. **Thelytokous**: The parthenogenetic production of female offspring from unfertilized eggs.

1. **Tier 1**. Tier 1 cell types were the highest-priority set and comprised three widely studied cell lines: K562 erythroleukaemia cells; GM12878, a B-lymphoblastoid cell line that is also part of the 1000 Genomes project (http://1000genomes.org)55; and the H1 embryonic stem cell (H1 hESC) line.

1. **Tier 2**. The second-priority set of cell types in the ENCODE project which included HeLa-S3 cervical carcinoma cells, HepG2 hepatoblastoma cells and primary (non-transformed) human umbilical vein endothelial cells (HUVECs).

1. **Tier 3**. Any other ENCODE cell types not in tier 1 or tier 2.

1. **Tiling array**. A microarray design in which the probes are selected to interrogate a genome with a consistent, pre-determined spacing between each probe.

1. **Topoisomerase**. An enzyme that can remove (or create) supercoiling and concatenation (interlocking) in duplex DNA by creating transitory breaks in one (type I topoisomerase) or both (type II topoisomerase) strands of the sugar–phosphate backbone.

1. **TR50** A measure of replication timing corresponding to the time in the cell cycle when 50% of the cells have replicated their DNA at a specific genomic position

1. **Transcriptomic**. Pertaining to the transcriptome, which is the combined set of RNA transcripts that are present in a given tissue at a given time.

1. **Transfer RNAs**. (tRNAs). Adaptor RNA molecules (76–90 nucleotides) that serve as the physical link between the mRNA and the amino acid sequence of proteins by carrying an amino acid to the ribosome, as directed by the codon in an mRNA. 

1. **Trophectoderm**. The outer layer of the blastocyst-stage embryo. 

1. **Transcription start site**. A nucleotide in the genome that is the first to be transcribed into a particular RNA.

1. **TxFrag Fragment of a transcript (TSS**): a genomic region found to be present in a transcript by an unbiased tiling-array assay.

1. **Type-1 error**. The rejection of a true null hypothesis; for example, concluding that HWE does not hold when in fact it does. By contrast, the power of a test is the probability of correctly rejecting a false null hypothesis. 

## U

1. **UK Biobank**: It is a large long-term biobank study in the United Kingdom which is investigating the respective contributions of genetic predisposition and environmental exposure to the development of disease. It began in 2006.

1. **Ultra-conserved elements**. Subsequences of the genome that appear to be under extremely high levels of sequence constraint based on phylogenetic comparisons. 

1. **Unbalanced rearrangement**. A genomic variant that involves gain or loss of DNA, such as deletion and duplication.

1. **Univariate analyses**: Tests of association between one phenotype and a genetic variant.

1. **Untaggable SNPs**. In Phase II HapMap, in spite of the high SNP density there are high-frequency SNPs for which no tag can be identified. Among hiIIgh-frequency SNPs (MAF > 0.2), we marked as untaggable SNPs to which no other SNP within 100 kb has an r2 value of at least 0.2.

1. **Un.TxFrag**: A TxFrag that is not associated with any other functional annotation

1. **Unphased diploid data**. Sequence data in which the phase of double heterozygotes was not determined.

1. **Untranslated region (UTR**): part of a cDNA either at the 5' or 3' end that does not encode a protein sequence

## V

1. **Variance**. A statistic that quantifies the dispersion of data about the mean.

1. **Variant call format files (VCF files)**. A flexible text file format developed within the 1000 Genomes Project that contains data specific to one or more genomic sites, including site coordinates, reference allele, observed alternative allele (or alleles) and base-call quality metrics.

1. **Variants of Uncertain Significance (VOUS)**. A variant of uncertain significance (VOUS) is a genetic sequence change whose association with disease risk is currently unknown. 

## W

1. **Whole-exome and targeted sequencing**. Sequencing of only exons or other selected regions. A system of capture or amplification is used to isolate or enrich for only exons or target regions. This is done by designing probes or primers for the regions of interest. 

1. **Whole-genome amplification (WGA**). Refers to methods that are used to amplify the genomic DNA of single cells to increase the number of copies of DNA for downstream processing. 

1. **Whole-genome sequencing (WGS)**. Sequencing of the entire genome without using methods for sequence selection. 

## Z

1. **Zero-mode waveguides (ZMW)**. Nanostructure devices used in the Pacific Biosciences (PacBio) platform. Each ZMW well (also called a waveguide) is several nanometres in diameter and is anchored to a glass substrate. The size of each well does not allow for light propagation, thus the fluorophores bound to bases can only be visualized through the glass substrate in the bottom-most portion of the well, a volume in the zeptolitre range. 
 
