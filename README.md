# Genómica Evolutiva y Poblacional

##   Links del curso: 

* :books: [Teóricas](https://www.dropbox.com/home/UBA/GenEvoPop)

* :computer: [TPs](https://gitlab.com/genevopop/tps_genevopop/blob/master/README.md)

* :open_file_folder:[Bibliografía](Files/Bibliografia_curso.md)

* :clipboard: [Glosario](Files/GEP_glosario.md)    

* :bird: [Twitter](https://twitter.com/hernandopazo) 

* :movie_camera: [Video Ofial del Curso](https://www.youtube.com/watch?v=t0DeJ5HeG8o&feature=youtu.be)

* :house: [Webpage](https://www.genevopop.net/grado)

____ 